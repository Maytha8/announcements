<define-tag pagetitle>Официальные информационные каналы Debian</define-tag>
<define-tag release_date>2020-03-16</define-tag>
#use wml::debian::news

<p>
Время от времени мы получаем вопросы относительно официальных информационных
каналов Debian и вопросы о статусе тех, кто владеет веб-сайтами со сходными
с Debian именами.
</p>

<p>
Основным источником информации является главный веб-сайт Debian,
<a href="https://www.debian.org">www.debian.org</a>. Тем, кто ищет информацию о текущих
событиях и прогрессе разработки в нашем сообществе, может быть интересен
раздел <a href="https://www.debian.org/News/">Новости Debian</a> веб-сайта
Debian.

Для менее формальных сообщений у нас имеется блог Debian
<a href="https://bits.debian.org">Bits from Debian</a>,
а также служба <a href="https://micronews.debian.org">микроновостей Debian</a>
для более коротких новостей.
</p>

<p>
Наше официальное информационное письмо
<a href="https://www.debian.org/News/weekly/">Новости Проекта Debian</a>
и все официальные сообщения о новостях или изменениях в Проекте одновременно публикуются
на нашем веб-сайте и рассылаются через наши официальные списки рассылки
<a href="https://lists.debian.org/debian-announce/">debian-announce</a> или
<a href="https://lists.debian.org/debian-news/">debian-news</a>.
Публикация сообщений в этих списках рассылки ограничена.
</p>

<p>
Кроме того, мы хотели бы сообщить, как Проект Debian,
или короче &mdash; Debian, структурирован.
</p>

<p>
Структура Debian регулируется нашей
<a href="https://www.debian.org/devel/constitution">Конституцией</a>.
Официальные лица и делегированные члены сообщества указаны на нашей странице,
описывающей <a href="www.debian.org/intro/organization">организационную структуру</a>.
Остальные команды указаны на вики-странице <a href="https://wiki.debian.org/Teams">Teams</a>.
</p>

<p>
Полный список официальных участников Debian можно найти на
<a href="https://nm.debian.org/members">странице новых участников</a>,
через которую осуществляется управление членством в Проекте. Более широкий список участников Debian
можно найти на нашей странице <a href="https://contributors.debian.org">Contributors</a>.
</p>

<p>
Если у вас возникли вопросы, то предлагаем вам связаться с командой по связям
с прессой по адресу &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
