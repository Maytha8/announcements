# Status: open-for-edit
# $Id$
# $Rev$
<define-tag pagetitle>Security support for Bullseye handed over to the LTS team</define-tag>
<define-tag release_date>2024-08-14</define-tag>

##
## Translators should uncomment the following line and add their name
## Leaving translation at 1.1 is okay; that's the first version which will
## be added to Debian's webwml repository
##
# ← this one must be removed; not that one → #use wml::debian::translation-check translation="1.1" maintainer=""

<p>
As of 14 August 2024, three years after the initial release, the regular
security support for Debian 11, alias <q>Bullseye</q>, comes to an end. The
Debian <a href="https://wiki.debian.org/LTS/">Long Term Support (LTS)</a> Team
will take over security support.
</p>

<h2>Information for users</h2>

<p>
Bullseye LTS will be supported from 15 August 2024 to 30 June 2026.
</p>

<p>
For how to use Debian Long Term Support please read
<a href="https://wiki.debian.org/LTS/Using">LTS/Using</a>.
</p>

<p>
Important information and changes regarding Bullseye LTS can be found at
<a href="https://wiki.debian.org/LTS/Bullseye">LTS/Bullseye</a>.
</p>

<p>
You should also subscribe to the
<a href="https://lists.debian.org/debian-lts-announce/">announcement mailing
list</a> for security updates for Bullseye LTS.
</p>

<p>
A few packages are not covered by the Bullseye LTS support. Non-supported
packages installed in the users machines can be detected by installing the
<a href="https://tracker.debian.org/pkg/debian-security-support">debian-security-support</a>
package. If debian-security-support detects an unsupported package which is
critical to you, please get in touch with
<strong>debian-lts@lists.debian.org</strong>.
</p>

<h2>Mailing lists</h2>

<p>
The whole coordination of the Debian LTS effort is handled through the
<a href="https://lists.debian.org/debian-lts/">debian-lts mailing list</a>.
</p>

<p>
Please subscribe or follow us via GMANE (gmane.linux.debian.devel.lts)
</p>

<p>
Aside from the debian-lts-announce list, there is also a <a
href="https://lists.debian.org/debian-lts-changes/">list for following all
uploads in Bullseye LTS</a>.
</p>

<h2>Security Tracker</h2>

<p>
All information on the status of vulnerabilities (e.g. if the version in
Bullseye LTS happens to be unaffected while Bookworm is affected) will be
tracked in the <a href="http://security-tracker.debian.org">Debian Security
Tracker</a>.
</p>

<p>
If you happen to spot an error in the data, please see
<a href="https://security-tracker.debian.org/tracker/data/report">https://security-tracker.debian.org/tracker/data/report</a>.
</p>

<h2>About Debian</h2>

##  Usually we use that version ...
<p>
The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
<q>universal operating system</q>.
</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.</p>

