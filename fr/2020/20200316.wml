#use wml::debian::translation-check translation="XXXXXX" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Les canaux de communication officiels de Debian</define-tag>
<define-tag release_date>2020-03-16</define-tag>
#use wml::debian::news

<p>
De temps à autre, nous sommes interrogés dans Debian sur nos canaux de
communication officiels et sur la position pour Debian de ceux qui peuvent
posséder des sites internet nommés de façon similaire.
</p>

<p>
Le site web principal de Debian <a href="https://www.debian.org">www.debian.org</a>
constitue notre moyen de communication fondamental. Tous ceux qui recherchent
des informations sur des événements actuels et le processus de développement
dans la communauté seront intéressés par la section des
<a href="https://www.debian.org/News/">actualités récentes</a> du site de
Debian.

Pour des annonces moins formelles, le projet dispose du blog officiel de Debian
<a href="https://bits.debian.org">Bits from Debian</a>,
et du service <a href="https://micronews.debian.org">Debian micronews</a>
pour les nouvelles brèves.
</p>

<p>
La lettre d'information officielle
<a href="https://www.debian.org/News/weekly/">Nouvelles du projet Debian</a>
et toutes les annonces officielles de nouvelles ou d'évolutions du projet sont
envoyées à la fois sur le site internet et aux listes de diffusion officiel du
projet
<a href="https://lists.debian.org/debian-announce/">debian-announce</a> ou
<a href="https://lists.debian.org/debian-news/">debian-news</a>.
Les envois à ces listes de diffusion sont modérés.
</p>

<p>
Nous profitons de cette annonce pour indiquer comment le Projet Debian, ou pour
faire court, Debian, est structuré.
</p>

<p>
La structure de Debian est réglementée par sa
<a href="https://www.debian.org/devel/constitution">Constitution</a>.
La liste des directeurs et des membres délégués se trouve sur la page
<a href="www.debian.org/intro/organization">Structure organisationnelle de Debian</a>.
D'autres équipes sont listées sur la page <a href="https://wiki.debian.org/Teams">Teams</a>
du wiki.
</p>

<p>
Une liste complète des membres officiels de Debian se trouve sur la
<a href="https://nm.debian.org/members">page des nouveaux membres</a>,
où est gérée l'affiliation au projet. Une liste plus large des contributeurs de
Debian se trouve sur la page des <a href="https://contributors.debian.org">Contributeurs</a>.
</p>

<p>
Si vous avez des questions, nous vous invitons à vous adresser à l'équipe
« presse » à l'adresse &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>
