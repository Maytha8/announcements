#use wml::debian::translation-check translation="21882a152ab072f844cf526dc172ff70559e05e7" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 10.1</define-tag>
<define-tag release_date>2019-09-07</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la première mise à jour de sa
distribution stable Debian <release> (codename <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version
de Debian <release> mais seulement une mise à jour de certains des paquets
qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de
la version <codename>. Après installation, les paquets peuvent être mis à
niveau vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette
mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP
de Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Corrections de bogues divers</h2>

<p>Cette mise à jour de la version stable apporte quelques corrections
importantes aux paquets suivants :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction acme-tiny "Traitement du changement du protocole ACME à venir">
<correction android-sdk-meta "Nouvelle version amont ; correction des expressions rationnelles pour l'ajout de la version Debian aux paquets binaires">
<correction apt-setup "Correction de la préconfiguration de Secure Apt pour les dépôts locaux au moyen de apt-setup/localX/">
<correction asterisk "Correction de dépassement de tampon dans res_pjsip_messaging [AST-2019-002/CVE-2019-12827] ; correction d'une vulnérabilité de plantage distant dans chan_sip [AST-2019-003/CVE-2019-13161]">
<correction babeltrace "Passage des dépendances des symboles ctf à la version après fusion">
<correction backup-manager "Correction de la purge d'archives distantes au moyen de FTP ou de SSH">
<correction base-files "Mise à jour pour cette version">
<correction basez "Décodage correct des chaînes encodées avec base64url">
<correction bro "Corrections de sécurité [CVE-2018-16807 CVE-2018-17019]">
<correction bzip2 "Correction d'une régression lors de la décompression de certains fichiers">
<correction cacti "Correction de certains problèmes lors de la mise à niveau à partir de la version dans Stretch">
<correction calamares-settings-debian "Correction des droits de l'image initramfs quand le chiffrement de disque complet est activé [CVE-2019-13179]">
<correction ceph "Reconstruction avec le nouveau libbabeltrace">
<correction clamav "Extraction de « bombes zip » non récursives évitées ; nouvelle version amont stable avec des corrections de sécurité – ajout de limite de temps de balayage pour réduire le risque de « bombes zip » [CVE-2019-12625] ; correction d'écriture hors limites dans la bibliothèque NSIS bzip2 [CVE-2019-12900]">
<correction cloudkitty "Correction d'échecs de construction avec SQLAlchemy mis à jour">
<correction console-setup "Correction de problèmes d'internationalisation lors du changement de locales dans Perl &gt;= 5.28">
<correction cryptsetup "Correction de la prise en charge des en-têtes LUKS2 sans aucune limite de « keyslot » ; correction de dépassement de segments mappés dans les architectures 32 bits">
<correction cups "Correction de multiples problèmes de sécurité ou de divulgation – problèmes de dépassements de tampon SNMP [CVE-2019-8696 CVE-2019-8675], de dépassement de tampon IPP, de déni de service et de divulgation de mémoire dans l'ordonnanceur">
<correction dbconfig-common "Correction d'un problème provoqué par la modification du comportement de bash POSIX">
<correction debian-edu-config "Utilisation de l'option <q>ipappend 2</q> de PXE pour l'amorçage plus rapide des clients LTSP ; correction de la configuration de sudo-ldap ; correction de la perte d'adresses IP v4 dynamiquement allouées ; plusieurs corrections et améliorations de debian-edu-config.fetch-ldap-cert">
<correction debian-edu-doc "Mise à jour des manuels et des traductions de Debian Edu Buster et d'ITIL">
<correction dehydrated "Correction de la récupération des informations de compte ; corrections complémentaires pour la gestion des identifiants de compte et pour la compatibilité APIv1">
<correction devscripts "debchange : cible buster-backports avec l'option --bpo">
<correction dma "Connexions TLS pas limitées à l'utilisation de TLS 1.0">
<correction dpdk "Nouvelle version amont stable">
<correction dput-ng "Ajout des noms de code buster-backports et stretch-backports-sloppy">
<correction e2fsprogs "Correction de plantages de e4defrag dans les architectures 32 bits">
<correction enigmail "Nouvelle version amont ; corrections de sécurité [CVE-2019-12269]">
<correction epiphany-browser "Utilisation garantie de la copie fournie de libdazzle par l'extension web">
<correction erlang-p1-pkix "Correction de la gestion des certificats GnuTLS">
<correction facter "Correction de l'analyse des options de routage Linux non clé/valeur (par exemple onlink)">
<correction fdroidserver "Nouvelle version amont">
<correction fig2dev "Plus d'erreur de segmentation sur les pointes de flèche circulaires ou semi-circulaires avec un grossissement supérieur à 42 [CVE-2019-14275]">
<correction firmware-nonfree "atheros : ajout du microprogramme Qualcomm Atheros QCA9377 rev 1.0 version WLAN.TF.2.1-00021-QCARMSWP-1 ; realtek : ajout du microprogramme Bluetooth Realtek RTL8822CU ; atheros : annulation du changement du microprogramme QCA9377 rev 1.0 dans 20180518-1 ; misc-nonfree : ajout du microprogramme pour les puces sans fil MediaTek MT76x0/MT76x2u, les puces Bluetooth MediaTek MT7622/MT7668, et du microprogramme GV100 signé">
<correction freeorion "Correction de plantage lors du chargement ou de la sauvegarde des données de jeu">
<correction fuse-emulator "Préférence du dorsal X11 à celui de Wayland ; affichage de l'icône de Fuse dans la fenêtre GTK et dans le dialogue « About »">
<correction fusiondirectory "Vérifications plus strictes des recherches LDAP ; ajout de la dépendance manquante à php-xml">
<correction gcab "Correction de corruption lors de l'extraction">
<correction gdb "Reconstruction avec le nouveau libbabeltrace">
<correction glib2.0 "Création, par le dorsal de configuration GKeyFile, de ~/.config et de fichiers de configuration avec des droits restreints [CVE-2019-13012]">
<correction gnome-bluetooth "Plus de plantage de GNOME Shell lors de l'utilisation de gnome-shell-extension-bluetooth-quick-connect">
<correction gnome-control-center "Correction de plantage quand le panneau Détails -&gt; Overview (info-overview) est sélectionné ; correction de fuites de mémoire dans le panneau Accès universel ; correction d'une régression qui empêchait les options Accès universel -&gt; Zoom mouse tracking d'avoir un effet ; mise à jour des traductions islandaise et japonaise">
<correction gnupg2 "Rétroportage de nombreuses corrections de bogue et de correctifs de stabilité à partir de l'amont ; utilisation de keys.openpgp.org comme serveur de clés par défaut ; importation uniquement des auto-signatures par défaut">
<correction gnuplot "Correction d'initialisation incomplète ou non sûre de tableau ARGV">
<correction gosa "Vérifications plus strictes des recherches LDAP">
<correction hfst "Mises à niveau à partir de Strecth sans heurt assurées">
<correction initramfs-tools "Désactivation de resume quand il n'y a pas de périphérique swap utilisable ; MODULES=most : inclusion de tous les modules de pilote de clavier, des pilotes cros_ec_spi et SPI et de extcon-usbc-cros-ec ; MODULES=dep : inclusion des pilotes extcon">
<correction jython "Conservation de la rétrocompatibilité avec Java 7">
<correction lacme "Mise à jour pour supprimer la prise en charge de GET non authentifié de l'API ACMEv2 de Let's Encrypt">
<correction libblockdev "Utilisation de l'API cryptsetup existante pour changer la phrase secrète de « key slot »">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libjavascript-beautifier-perl "Ajout de la prise en charge de l'opérateur <q>=&gt;</q>">
<correction libsdl2-image "Correction de dépassements de tampon [CVE-2019-5058 CVE-2019-5052 CVE-2019-7635] ; correction d'accès hors limites dans la gestion de PCX [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction libtk-img "Arrêt de l'utilisation de copies internes des codecs JPEG, Zlib et PixarLog, corrigeant des plantages">
<correction libxslt "Correction de contournement de l'infrastructure de sécurité [CVE-2019-11068], lecture non initialisée de jeton xsl:number [CVE-2019-13117] et lecture non initialisée de caractères groupés UTF-8 [CVE-2019-13118]">
<correction linux "Nouvelle version amont stable">
<correction linux-latest "Mise à jour pour l'ABI du noyau 4.19.0-6">
<correction linux-signed-amd64 "Nouvelle version amont stable">
<correction linux-signed-arm64 "Nouvelle version amont stable">
<correction linux-signed-i386 "Nouvelle version amont stable">
<correction lttv "Reconstruction avec le nouveau libbabeltrace">
<correction mapproxy "Correction des capacités WMS avec Python 3.7">
<correction mariadb-10.3 "Nouvelle version amont stable ; corrections de sécurité [CVE-2019-2737 CVE-2019-2739 CVE-2019-2740 CVE-2019-2758 CVE-2019-2805] ; correction d'erreur de segmentation lors de l'accès à « information_schema » ; « mariadbcheck » renommé « mariadb-check »">
<correction musescore "Désactivation de la fonctionnalité webkit">
<correction ncbi-tools6 "Réempaquetage sans les données non libres d'UniVec.*">
<correction ncurses "Suppression de <q>rep</q> des descriptions de xterm-new et du terminfo dérivé">
<correction netdata "Suppression de Google Analytics de la documentation générée ; renonciation à l'envoi de statistiques anonymes ; suppression du bouton <q>sign in</q>">
<correction newsboat "Correction de problème d'utilisation de mémoire après libération">
<correction nextcloud-desktop "Ajout de la dépendance manquante de nextcloud-desktop-common à nextcloud-desktop-cmd">
<correction node-lodash "Correction de pollution de prototype [CVE-2019-10744]">
<correction node-mixin-deep "Correction d'un problème de pollution de prototype">
<correction nss "Correction de problèmes de sécurité [CVE-2019-11719 CVE-2019-11727 CVE-2019-11729]">
<correction nx-libs "Correction d'un certain nombre de fuites de mémoire">
<correction open-infrastructure-compute-tools "Correction de démarrage de conteneur">
<correction open-vm-tools "Gestion correcte des versions de système d'exploitation sous la forme <q>X</q>, plutôt que <q>X.Y</q>">
<correction openldap "Limitation de proxyauthz de rootDN à ses propres bases de données [CVE-2019-13057] ; déclaration d'ACL sasl_ssf exigée à chaque connexion [CVE-2019-13565] ; correction de slapo-rwm pour ne pas vider le filtre original quand le filtre réécrit n'est pas valable">
<correction osinfo-db "Ajout des informations sur Buster 10.0 ; correction des URL pour le téléchargement de Stretch ; correction du nom du paramètre utilisé pour la configuration du nom complet lors de la création d'un fichier de préconfiguration">
<correction osmpbf "Reconstruction avec protobuf 3.6.1">
<correction pam-u2f "Correction de gestion de fichier de débogage non sûre [CVE-2019-12209] ; correction de fuite de descripteur de fichier de débogage [CVE-2019-12210] ; correction d'accès hors limites ; correction d'erreur de segmentation consécutif à un échec d'allocation de tampon">
<correction passwordsafe "Installation des fichiers de localisation dans le répertoire correct">
<correction piuparts "Mise à jour des configurations pour la version de Buster ; correction d'un faux échec à la suppression de paquets dont le nom se termine par « + » ; création de noms d'archive distincts pour les chroots --merged-usr">
<correction postgresql-common "Correction de <q>pg_upgradecluster from postgresql-common 200, 200+deb10u1, 201, and 202 will corrupt the data_directory setting when used *twice* to upgrade a cluster (e.g. 9.6 -&gt; 10 -&gt; 11)</q>">
<correction pulseaudio "Correction de restauration de l'état muet">
<correction puppet-module-cinder "Correction de tentatives d'écriture dans /etc/init">
<correction python-autobahn "Correction des dépendances de construction de pyqrcode">
<correction python-django "Nouvelle version de sécurité amont [CVE-2019-12781]">
<correction raspi3-firmware "Ajout de la prise en charge de Raspberry Pi Compute Module 3 (CM3), Raspberry Pi Compute Module 3 Lite, et de Raspberry Pi Compute Module IO Board V3">
<correction reportbug "Mise à jour des noms de version, suite à la publication de Buster ; réactivation des requêtes stretch-pu ; correction de plantages lors de la recherche de paquet ou de version ; ajout de la dépendance manquante à sensible-utils">
<correction ruby-airbrussh "Pas d'envoi d'exception lors de sortie SSH UTF-8 non valable">
<correction sdl-image1.2 "Correction de dépassements de tampon [CVE-2019-5052 CVE-2019-7635] et d'accès hors limites [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction sendmail "sendmail-bin.postinst, initscript : correspondance de start-stop-daemon avec un pidfile et un exécutable ; sendmail-bin.prerm : arrêt de sendmail avant la suppression des alternatives">
<correction slirp4netns "Nouvelle version amont stable avec des corrections de sécurité – vérification du résultat de sscanf lors de l'émulation d'ident [CVE-2019-9824] ; corrections de dépassement de tas dans la version de libslirp incluse [CVE-2019-14378]">
<correction systemd "Network : correction d'échec d'activation d'interface avec le noyau Linux 5.2 ; ask-password : dépassement de tampon évité lors de la lecture à partir du trousseau ; network : comportement plus élégant lorsqu'IPv6 a été désactivé">
<correction tzdata "Nouvelle version amont">
<correction unzip "Correction de problèmes de « bombe zip » [CVE-2019-13232]">
<correction usb.ids "Mise à jour ordinaire des identifiants USB">
<correction warzone2100 "Correction d'une erreur de segmentation lors de l'hébergement d'un jeu multijoueur">
<correction webkit2gtk "Nouvelle version amont stable ; plus d'exigence de processeur utilisant SSE2">
<correction win32-loader "Reconstruction avec les paquets actuels, particulièrement de debian-archive-keyring ; correction d'échec de construction en imposant une « locale » POSIX">
<correction xymon "Correction de plusieurs problèmes de sécurité (serveur uniquement) [CVE-2019-13273 CVE-2019-13274 CVE-2019-13451 CVE-2019-13452 CVE-2019-13455 CVE-2019-13484 CVE-2019-13485 CVE-2019-13486]">
<correction yubikey-personalization "Rétroportage de mesures de sécurité supplémentaires">
<correction z3 "Pas de configuration du SONAME de libz3java.so à libz3.so.4">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2019 4477 zeromq3>
<dsa 2019 4478 dosbox>
<dsa 2019 4479 firefox-esr>
<dsa 2019 4480 redis>
<dsa 2019 4481 ruby-mini-magick>
<dsa 2019 4482 thunderbird>
<dsa 2019 4483 libreoffice>
<dsa 2019 4484 linux>
<dsa 2019 4484 linux-signed-i386>
<dsa 2019 4484 linux-signed-arm64>
<dsa 2019 4484 linux-signed-amd64>
<dsa 2019 4486 openjdk-11>
<dsa 2019 4488 exim4>
<dsa 2019 4489 patch>
<dsa 2019 4490 subversion>
<dsa 2019 4491 proftpd-dfsg>
<dsa 2019 4493 postgresql-11>
<dsa 2019 4494 kconfig>
<dsa 2019 4495 linux-signed-amd64>
<dsa 2019 4495 linux-signed-arm64>
<dsa 2019 4495 linux>
<dsa 2019 4495 linux-signed-i386>
<dsa 2019 4496 pango1.0>
<dsa 2019 4498 python-django>
<dsa 2019 4499 ghostscript>
<dsa 2019 4501 libreoffice>
<dsa 2019 4502 ffmpeg>
<dsa 2019 4503 golang-1.11>
<dsa 2019 4504 vlc>
<dsa 2019 4505 nginx>
<dsa 2019 4507 squid>
<dsa 2019 4508 h2o>
<dsa 2019 4509 apache2>
<dsa 2019 4510 dovecot>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction pump "Non maintenu ; problèmes de sécurité">
<correction rustc "Suppression du code périmé de rust-doc">

</table>

<h2>Installateur Debian</h2>
<p>L'installateur a été mis à jour pour inclure les correctifs incorporés
dans cette version de stable.</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Adresse de l'actuelle distribution stable :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://security.debian.org/</a>
</div>


<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de
publication de la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>
