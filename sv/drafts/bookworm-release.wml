<define-tag pagetitle>Debian 12 <q>Bookworm</q> utgiven</define-tag>
<define-tag release_date>2023-06-10</define-tag>
#use wml::debian::news

<p>Efter 1 år, 9 månader och 28 dagars utveckling presenterar Debianprojektet
stolt sin nya stabila utgåva 12 (med kodnamnet <q>Bookworm</q>).</p>

<p><q>Bookworm</q> kommer att stödjas under de kommande 5 åren tack vare
gemensamt arbete mellan <a href="https://security-team.debian.org/">Debians säkerhetsgrupp</a>
och gruppen för <a href="https://wiki.debian.org/LTS">Debians långtidsstöd</a>.</p>

<p>Efter <a href="$(HOME)/vote/2022/vote_003">2022 års allmänna beslut om icke-fri fastprogramvara</a>,
har vi introducerat en ny arkivdel som gör det möjligt att separera icke-fri
fastprogramvara från andra icke-fria paket:
<ul>
<li>non-free-firmware</li>
</ul>

De flesta paket i non-free firmware har flyttats från <b>non-free</b> till
<b>non-free-firmware</b>. Denna uppdelning gör det möjligt att bygga en
mängd olika officiella installationsavbildningar.
</p>

<p>Debian 12 <q>Bookworm</q> släpps med många skrivbordmiljöer som:
</p>
<ul>
<li>Gnome 43,</li>
<li>KDE Plasma 5.27,</li>
<li>LXDE 11,</li>
<li>LXQt 1.2.0,</li>
<li>MATE 1.26,</li>
<li>Xfce 4.18</li>
</ul>

<p>Denna utgåva innehåller mer än <b>11089</b> nya paket vilket ger en total på
<b>64419</b> paket, medan över <b>6296</b> paket har tagits bort eftersom de sågs
som <q>obsoleta</q>. <b>43254</b> paket har uppdaterats i
denna utgåva.

Totala diskanvändningen för <q>Bookworm</q> är <b>365,016,420 kB (365 GB)</b>, och
detta motsvarar <b>1,341,564,204</b> rader kod.</p>

<p><q>Bookworm</q> har flera översatta man-sidor än någonsin tack vare våra
översättare som har gjort <b>man</b>-sidor tillgängliga i flera språk så
som: tjeckiska, danska, grekiska, finska, indonesiska, makedonska,
norska (bokmål), ryska, serbiska, svenska, ukrainska och vietnamesiska.
Samtliga <b>systemd</b>-man-sidor finns nu tillgängliga på tyska.</p>

<p>Debian Med-blandningen introducerar ett nytt paket: <b>shiny-server</b> som
förenklar vetenskapliga webbapplikationer med hjälp av <b>R</b>. Vi har
använt våra ansträngningar för att tillhandahålla stöd för Continuous
Integration för paket från Debian Med-gruppen. Installera metapaketen vid
version 3.8.x för Debian Bookworm.</p>

<p>Debian Astro-blandningen fortsätter att tillhandahålla en enkel lösning för
professionella astronomer, entusiaster och hobbyister med uppdateringar till
nästan alla versioner av mjukvarupaketen i blandningen. <b>astap</b> och
<b>planetary-system-stacker</b> hjälper till med bildstapling och astrometrilösning.
Även <b>openvlbi</b>, öppen-källkodskorrelatorn inkluderas nu.</p>

<p>Stöd för Secure Boot på ARM64 har återintroducerats - användare av UEFI-kapabel
ARM64-hårdvara kan starta med Secure Boot-läge aktiverat för att dra full nytta
av säkerhetsfunktionen.</p>

<p>Debian 12 <q>Bookworm</q> inkluderar flera uppdaterade mjukvarupaket
(mer än 67% av alla paket från föregående utgåva), så som:

<ul>
<li>Apache 2.4.57</li>
<li>BIND DNS Server 9.18</li>
<li>Cryptsetup 2.6</li>
<li>Dovecot MTA 2.3.19</li>
<li>Emacs 28.2</li>
<li>Exim (standard e-postserver) 4.96</li>
<li>GIMP 2.10.34</li>
<li>GNU Compiler Collection 12.2</li>
<li>GnuPG 2.2.40</li>
<li>Inkscape 1.2.2</li>
<li>The GNU C Library 2.36</li>
<li>lighthttpd 1.4.69</li>
<li>LibreOffice 7.4</li>
<li>Linux kernel 6.1 series</li>
<li>LLVM/Clang toolchain 13.0.1, 14.0 (standard), och 15.0.6</li>
<li>MariaDB 10.11</li>
<li>Nginx 1.22</li>
<li>OpenJDK 17</li>
<li>OpenLDAP 2.5.13</li>
<li>OpenSSH 9.2p1</li>
<li>Perl 5.36</li>
<li>PHP 8.2</li>
<li>Postfix MTA 3.7</li>
<li>PostgreSQL 15</li>
<li>Python 3, 3.11.2</li>
<li>Rustc 1.63</li>
<li>Samba 4.17</li>
<li>systemd 252</li>
<li>Vim 9.0</li>
</ul>
</p>

<p>
Med detta breda urval av paket och dess traditionellt breda
arkitektursstöd, håller sig Debian till sitt mål att vara det
universella operativsystemet. Det passar många olika användningsområden:
från skrivbordssystem till laptops; från utvecklingsservrar till
klustersystem; och för databaser, webb, eller lagringsservrar. På samma
gång säkerställer ytterligare kvalitetssäkringsinsatser, så som automatiska
installations- och uppgraderingstester för alla paket i Debians arkiv, att
<q>Bookworm</q> uppfyller de höga förväntningarna som användare har på en
stabil Debianutgåva.
</p>

<p>
Totalt nio arkitekturer stöds officiellt i <q>Bookworm</q>:
<ul>
<li>32-bitars PC (i386) och 64-bitars PC (amd64),</li>
<li>64-bitars ARM (arm64),</li>
<li>ARM EABI (armel),</li>
<li>ARMv7 (EABI hard-float ABI, armhf),</li>
<li>little-endian MIPS (mipsel),</li>
<li>64-bitars little-endian MIPS (mips64el),</li>
<li>64-bitars little-endian PowerPC (ppc64el),</li>
<li>IBM System z (s390x)</li>
</ul>

32-bitars PC (i386) stödjer inte längre några i586-processorer, minimumkravet
för processorer är i686. <i>Om din maskin inte är kompatibel med detta krav
rekommenderas att du stannar på Bullseye för den kvarvarande tiden av dess
supportcykel.</i>
</p>

<p>Debians molngrupp publicerar <q>Bookworm</q> för flera molntjänster:
<ul>
<li>Amazon EC2 (amd64 and arm64),</li>
<li>Microsoft Azure (amd64),</li>
<li>OpenStack (generic) (amd64, arm64, ppc64el),</li>
<li>GenericCloud (arm64, amd64),</li>
<li>NoCloud (amg64, arm64, ppc64el)</li>
</ul>

genericcloud-avbildningen ska kunna köras i vilken virtualiserad miljö som
helst, och det finns även en nocloud-avbildning som är användbar för att
testa byggprocessen.
</p>

<p>GRUB-paket kommer som standard <a href="https://www.debian.org/releases/bookworm/amd64/release-notes/ch-information.en.html#grub-os-prober">inte längre köra os-prober för andra operativsystem.</a>

<p>Mellan utgåvor har tekniska kommittèn beslutat att Debian <q>Bookworm</q>
ska <a href="$(HOME)/UsrMerge">stödja endast root-filsystemlayout med sammanslagen-usr</a>,
och därmed överge stöd för icke-sammanslagen-usr layout. För system som
installerats som Buster eller Bullseye kommer det inte att ske några skillnader
i filsystemet, men system som använder den äldre layouten kommer att
konverteras under uppgraderingen.</p>


</p>
<h3>Vill du prova?</h3>
<p>
Om du helt enkelt vill testa Debian 12 <q>Bookworm</q> utan att installera det,
kan du använda en av de tillgängliga <a href="$(HOME)/CD/live/">live-avbildningarna</a> som laddar och kör
det fullständiga operativsystemet i ett skrivskyddat läge i din dators minne.
</p>

<p>
Dessa live-avbildningar tillhandahålls för arkitekturerna <code>amd64</code> och
<code>i386</code> och finns tillgängliga för DVD-skivor, USB-minnen och
nätverksstart-setups. Användaren kan välja mellan olika skrivbordsmiljöer att
testa: GNOME, KDE Plasma, LXDE, LXQt, MATE och Xfce. Debian Live
<q>Bookworm</q> har en standardliveavbildning, så det är även möjligt att
prova ett bassystem av Debian utan något grafiskt användargränssnitt.
</p>

<p>
Om du trivs med operativsystemet har du alternativet att installera från
live-avbildningen till din dators hårddisk. Live-avbildningen inkluderar
den oberoende Calamares-installeraren så väl som den vanliga Debian-installeraren.
Ytterligare information finns tillgänglig i
<a href="$(HOME)/releases/bookworm/releasenotes">versionsfakta</a> samt
<a href="$(HOME)/CD/live/">avsnittet för liveinstallationsavbildningar
på Debians webbplats</a>.
</p>


<p>
För att installera Debian 12 <q>Bookworm</q> direkt på din dators lagringsenhet
kan du välja mellan ett antal olika typer av installationsmedia att
<a href="https://www.debian.org/download">hämta</a> så som
Blu-ray, DVD, CD, USB-minne, eller via en nätverksanslutning.
Se <a href="$(HOME)/releases/bookworm/installmanual">installationsguiden</a> för ytterligare detaljer.
</p>

# Translators: some text taken from:

<p>
Debian kan nu installeras i 78 språk, med de flesta av dem tillgängliga
med både text-baserat och grafiskt användargränssnitt.
</p>

<p>
Installationsavbildningar kan hämtas redan nu via
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (den rekommenderade metoden),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, or
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; se
<a href="$(HOME)/CD/">Debian på CD</a> för ytterligare information. <q>Bookworm</q> kommer
snart att även finnas tillgängligt på fysisk DVD, CD-ROM och Blu-ray Disc från
ett antal <a href="$(HOME)/CD/vendors">försäljare</a>.
</p>


<h3>Uppgradera Debian</h3>
<p>
Uppgraderingar till Debian 12 <q>Bookworm</q> från den föregående utgåvan
Debian 11 <q>Bullseye</q> hanteras automatiskt av pakethanteringsverktyget
APT för de flesta konfigurationer.
</p>

<p>Före du uppgraderar ditt system rekommenderas det starkt att du gör en
fullständig backup, eller åtminstone att du gör backup på sådan data eller
konfigurationsinformation som du inte kan kosta på dig att förlora.
Uppgraderingsverktygen och processen är pålitlig, men ett hårdvarufel mitt
i en uppgradering kan resultera i ett allvarligt skadat system.

De främsta sakerna som du bör göra backup på är innehållet i /etc,
/var/lib/dpkg, /var/lib/apt/extended_states och utdata från:

<code>$ dpkg --get-selections '*' # (the quotes are important)</code>
</p>

<p>Vi välkomnar all information från användare relaterat till upgradering från
<q>Bullseye</q> till <q>Bookworm</q>. Vänligen dela information genom att
rapportera ett fel i
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-about.en.html#upgrade-reports">Debians
felspårningsystem</a> och använd paketet <b>upgrade-reports</b> med dina resultat.</p>


<p>
Det har skett en hel del utveckling i Debianinstalleraren vilket resulterar
i förbättrat hårdvarustöd och andra funktioner så som rättningar till
grafiskt stöd på UTM, rättningar till GRUBs typsnittsladdare, borttagning
av den långa vänteperioden vid slutet av installationsprocessen, och rättningar
till detekteringen av BIOS-bootbara system. Denna version av Debianinstalleraren
kan aktivera icke-fri-fastprogramvara följdaktligen.</p>



<p>
Paketet <b>ntp</b> har ersatts av paketet <b>ntpsec</b>,
med standard systemklocktjänsten som nu är <b>systemd-timesyncd</b>, medan det
fortfarande finns stöd även för <b>chrony</b> och <b>openntpd</b>.
</p>


<p>Eftersom <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#non-free-split"> <b>icke-fri</b>
fastprogramvara flyttades till sin egen komponent i arkivet</a>, om du har icke-fri
fastprogramvara installerad, rekommenderas det att lägga till <b>non-free-firmware</b>
till din APT sources-list.</p>

<p>Det rekommenderas att ta bort inlägg för bullseye-backports från dina
APT källlistefiler före uppgraderingen, och överväg att lägga till <b>bookworm-backports</b>
efter.</p>


<p>
För <q>Bookworm</q> är säkerhetssviten namngiven <b>bookworm-security</b> och
användare bör anpassa sina APT-källliste-filer därefter vid uppgradering.

Om din APT-konfiguration även innefattar fastnålning eller <code>APT::Default-Release</code>,
är det troligt att det kräver justeringar för att tillåta uppgradering av paket
till den nya stabila utgåvan, vänligen överväg att
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#disable-apt-pinning">inaktivera APT-fastnålning</a>.</p>


<p>OpenLDAP 2.5-uppgraderingen innehåller några <a href="https://www.debian.org/releases/testing/i386/release-notes/ch-information.en.html#openldap-2.5">inkompatibla ändringar
som kan kräva manuella ingrepp</a>, beroende på konfiguration kan
<b>slapd</b>-tjänsten förbli stoppad efter uppgraderingen tills nya
konfigurationsuppdateringar har slutförts.</p>


<p>Det nya paketet <b>systemd-resolved</b> kommer inte att installeras
automatiskt vid uppgraderingar eftersom det
<a href="r$(HOME)/eleases/testing/i386/release-notes/ch-information.en.html#systemd-resolved">har delats upp i ett separat paket</a>.
Om du använder systemtjänsten systemd-resolved, vänligen installera det nya paketet
för hand efter uppgraderingen, och notera att DNS-uppslagning inte fungerar
förrän tjänsten är tillgänglig på systemet.</p>



<p>Det finns några <a href="$(HOME)/releases/bookworm/amd64//release-notes/ch-information.en.html#changes-to-system-logging">ändringar till systemloggning</a>, 
paketet <b>rsyslog</b> behövs inte på de flesta system och installeras därmed
inte heller som standard. Användare kan byta till <b>journalctl</b> eller använda
"högprecisionstidsstämplar" som <b>rsyslog</b> numer använder.
</p>


<p><a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#trouble">Möjliga problem under uppgraderingen</a> inkluderar
Conflicts eller Pre-Depends-loopar kan lösas genom att ta bort och eliminera
vissa paket eller tvinga om-installation av andra paket. Ytterligare problem
är "Kunde inte utföra omedelbar konfiguration ..."-fel för vilka man måste
behålla <b>både</b> <q>bullseye</q> (som precis togs bort) och <q>bookworm</q>
(som precis lades till) i APTs källlistefil, och filkonflikter som kan
kräva att man använder funktionaliteten att tvinga bort paket.
Som nämnts är säkerhetskopiering av systemet nyckeln till en smidig uppgradering
om några ogynnsamma fel uppstår.</p>


<p>Det finns några paket där Debian inte kan lova att tillhandahålla minimala
bakåtanpassningar av säkerhetsfixar, vänligen se <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#limited-security-support">Begränsningar av säkerhetsstöd</a>.</p>


<p>
Som alltid kan Debiansystem uppgraderas smärtfritt, på plats,
utan något påtvingat stillestånd, men det rekommenderas starkt att läsa
<a href="$(HOME)/releases/bookworm/releasenotes">versionsfakta</a> så
väl som <a href="$(HOME)/releases/bookworm/installmanual">installationsguiden</a>
för möjliga problem, och för detaljerade instruktioner om installationen och
uppgradering. Versionsfakta kommer att förbättras och översättas till
ytterligare språk under veckorna efter utgåvan.
</p>


<h2>Om Debian</h2>

<p>
Debian är ett fritt operativsystem, utvecklat av
tusentals frivilliga från hela världen som samarbetar via Internet.
Debian-projektets styrkor är dess volontärbas, dess hängivenhet till
Debians Sociala kontrakt och fri mjukvara, och dess åtagande att
tillhandahålla det bästa operativsystemet möjligt. Denna nya
utgåva är ytterligare ett viktigt steg i denna riktning.
</p>


<h2>Kontaktinformation</h2>

<p>
För ytterligare information, var vänlig besök Debians webbsidor på
<a href="$(HOME)/">https://www.debian.org/</a> eller skicka e-post
(på engelska) till &lt;press@debian.org&gt;.
</p>
